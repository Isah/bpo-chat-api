package com.bposeats.bpochatapi.entities;

public class ChatUser {
	
	private String username;
	private String phoneNumber;
	
	public ChatUser(String username, String phoneNumber) {
		
		this.username = username;
		this.phoneNumber = phoneNumber;
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
	

}
